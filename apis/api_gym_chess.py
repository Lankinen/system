from system.agent import Agent
from system.api import API

from typing import Tuple, List, Optional
import gym


class GymChessAPI(API):
    env: gym.Env
    current_state: str

    ### required methods ###

    def __init__(self, number_of_agents: int):
        super().__init__(number_of_agents)
        if number_of_agents > 1:
            raise("This is one agent game for now")
        self.env = gym.make("Chess-v0")
        self.current_state = self.env.reset()

    def get_state(self, agent: Agent) -> str:
        super().get_state(agent)
        return "".join(str(self.current_state).split())

    def get_action_options(self, agent: Agent) -> List[str]:
        super().get_action_options(agent)
        return list(map(str, self.env.legal_moves))

    def do_action(self, agent: Agent, action_index: int) -> Tuple[int, Optional[str]]:
        super().do_action(agent, action_index)
        action = self.env.legal_moves[action_index]
        next_step, reward, is_terminated, info = self.env.step(action)
        self.current_state = next_step
        agent.is_terminated = is_terminated
        return reward, self.current_state

    def reset(self, agents: List[Agent]):
        super().reset(agents)
        self.current_state = self.env.reset()

    ### custom methods ###
