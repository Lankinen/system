from system.api import API
from system.agent import Agent
from system.utils import arg_max
from games.simple_slot import Casino, SimpleSlot

from typing import List, Tuple, Optional

class SimpleSlotAPI(API):
    simple_slots: List[SimpleSlot]

    ### required methods ###

    def __init__(self, number_of_agents: int):
        super().__init__(number_of_agents)
        print("number_of_agents:",number_of_agents)
        self.simple_slots = []
        for _ in range(number_of_agents):
            self.simple_slots.append(SimpleSlot(number_of_casinos=3))

    def get_state(self, agent: Agent) -> str:
        super().get_state(agent)
        return str(self.simple_slots[agent.uuid].get_casino())

    def get_action_options(self, agent: Agent) -> List[str]:
        super().get_action_options(agent)
        current_casino: Casino = self.simple_slots[agent.uuid].get_casino()
        number_of_actions = len(current_casino.levers)
        return list(map(str, range(number_of_actions)))

    def do_action(self, agent: Agent, action_index: int) -> Tuple[int, Optional[str]]:
        super().do_action(agent, action_index)
        simple_slot = self.simple_slots[agent.uuid]
        
        balance_before: int = simple_slot.balance
        simple_slot.play_in_casino(action_index)
        balance_after: int = simple_slot.balance
        
        agent.is_terminated = True

        return balance_after - balance_before, None
    
    def reset(self, agents: List[Agent]):
        super().reset(agents)
    
    def print_environment_initial_data(self) -> None:
        for simple_slot_index, simple_slot in enumerate(self.simple_slots):
            print("simple slot:",simple_slot_index)
            for casino in simple_slot.casinos:
                print("> casino:",casino.name,"->",len(casino.levers))
                averages = []
                for lever_index, lever in enumerate(casino.levers):
                    lever_avg = (lever[1]+lever[0])/2
                    averages.append(lever_avg)
                print("  -> averages:",averages)
                print("  arg max averages:", arg_max(averages))
        print("-"*20)

    ### custom methods ###

    ### WARINGIN! Never use this to decide the action! ###
    def get_optimal_actions(self, agent: Agent) -> List[int]:
        current_casino = self.simple_slots[agent.uuid].get_casino()
        a = []
        for lever in current_casino.levers:
            mn = lever[0]
            mx = lever[1]
            a.append((mn+mx) / 2)
        return arg_max(a, value=False)