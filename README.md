# The System
**Mission:** Allow easily test algorithms and environments minimizing the amount of code needed to change. This tool simplifies comparison of algorithms in different environments.

## Usage
### Setup
```
pip install -r requirements.txt
```

### Run
```
python main.py
```

### Tests
```
python -m unittest
```

### Changing algorithms/games
The System is designed in a way that `main.py` is the only place needed to modify when changing environments and/or algorithms.

## Development
### Git
Create a branch in git for every new environment and agent. If something needs to be done to the other files, it should be done into the `master` branch.

### Structure
`apis` directory contains APIs for each environment. All of the APIs inherit `API` class which force them to offer common API inside the code no matter of the environment.
`agents` directory contains different algorithms.
`games` the idea in this project was to make it easy to use any 3rd-party environments but I decided to make as a demo purpose simple environment.
`system` the core elements. These are not touched when adding environments, algorithms, or testing them.
`test` tests for the project.

### Adding Environment
All environments needs to inherit `API` class which is defined inside `system > api.py`. `API` explains the parameters and return value(s). Look other examples for more help.

### Adding Algorithm
All algorithms needs to inherit `Agent` class which is defined inside `system > agent.py`. `Agent` explains the parameters and return value(s). Look other examples for more help.
