from system.rl import RL
from system.utils import print_progress
from system.agent import Agent

from typing import Dict, List

from agents.agent_epsilon_explorer import EpsilonExplorer
from apis.api_simple_slot import SimpleSlotAPI
from apis.api_gym_chess import GymChessAPI

OptimalActions = Dict[int, List[int]]
number_of_optimal_chosen: Dict[int, List[int]] = {}


def before_episode_callback(episode: int, **kwargs) -> None:
    print_progress(episode, NUMBER_OF_EPISODES, PRINT_PROGRESS_INTERVAL)


def after_episode_callback(duration: float, episode: int, **kwargs) -> None:
    if episode % PRINT_PROGRESS_INTERVAL == 0:
        print("episode duration (seconds):", duration)


def before_step_callback(optimal_actions: OptimalActions, agent: Agent, **kwargs) -> None:
    optimal_actions[agent.uuid] = api.get_optimal_actions(agent)


def after_step_callback(optimal_actions: OptimalActions, action_index: int, agent: Agent, **kwargs) -> None:
    is_optimal_action = action_index in optimal_actions[agent.uuid]
    if agent.uuid not in number_of_optimal_chosen:
        number_of_optimal_chosen[agent.uuid] = []
    number_of_optimal_chosen[agent.uuid].append(is_optimal_action)


if __name__ == "__main__":
    NUMBER_OF_EPISODES: int = 10000
    PRINT_PROGRESS_INTERVAL: int = 100
    PLOT_PERFORMANCE: bool = False

    optimal_actions: OptimalActions = {}

    agents: List[Agent] = [EpsilonExplorer(0.05)]
    api: SimpleSlotAPI = SimpleSlotAPI(len(agents))
    # api: API = GymChessAPI(len(agents))

    before_step_lambda = lambda **kwargs: before_step_callback(
        optimal_actions=optimal_actions, **kwargs)

    after_step_lambda = lambda **kwargs: after_step_callback(
        optimal_actions=optimal_actions, **kwargs)

    rl = RL(agents, api, before_episode=before_episode_callback, after_episode=after_episode_callback,
            before_step=before_step_lambda, after_step=after_step_lambda)
    rl.start(NUMBER_OF_EPISODES, PLOT_PERFORMANCE)

    print("-"*20)
    for values in number_of_optimal_chosen.values():
        print(sum(values)/NUMBER_OF_EPISODES)
