from abc import ABC, abstractmethod
from typing import Tuple, List, Callable, Optional

class Agent(ABC):
    uuid: int = -1
    is_terminated: bool

    def __init__(self):
        self.uuid += 1
        self.is_terminated = False

    @abstractmethod
    def do_step(self, state: str, action_options: List[str], do_action: Callable[[int],Tuple[int, Optional[str]]]) -> Tuple[int, str, int]:
        """Choose action for given state

        Args:
            state (str): state
            action_options (List[str]): List of possible actions (might not be the same as all possible actions in the environment)
            do_action (Callable[[int],Tuple[int, Optional[str]]]): Callback that takes action index (from actions_options) and gives back reward and next state (optional).

        Returns:
            Tuple[int, str, int]: reward, selected action, index of selected action
        """
        assert type(state) is str
        assert type(action_options) is list
        for a in action_options:
            assert type(a) is str
        assert callable(do_action)
        return 3, "e", 13