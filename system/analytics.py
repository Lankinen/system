from system.utils import create_plot
from typing import Any, Dict, List

def plot_agent_performance(agent_index: int, agent_data: Dict) -> None:
    for y_label, items in agent_data.items():
        time_steps: List[int] = list(range(len(items)))

        create_plot(time_steps, items, "time step", y_label, "Agent: " + str(agent_index))