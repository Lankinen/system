from system.agent import Agent
from system.api import API
from system.analytics import plot_agent_performance

from typing import Callable, Dict, List, Optional, Tuple
import time

AgentWrappers = List[Dict]
AgentData = Dict

class RL():
    agent_wrappers: AgentWrappers
    api: API
    callbacks: Dict

    def __init__(self, agents, api, **callbacks):
        self.agent_wrappers = []
        for agent in agents:
            self.agent_wrappers.append({
                "agent": agent,
                "data": {}
            })
        self.api = api
        self.callbacks = callbacks

    def _take_one_step(self, agent: Agent, agent_data: AgentData) -> Tuple[int, str, int]:
        state = self.api.get_state(agent)
        action_options = self.api.get_action_options(agent)

        do_action_lambda: Callable[[int],Tuple[int, Optional[str]]] = lambda action_index : self.api.do_action(agent, action_index)

        reward, action, action_index = agent.do_step(state, action_options, do_action = do_action_lambda)

        return reward, action, action_index

    def _continue_until_all_agents_is_terminated(self) -> List[int]:
        episode_rewards: List[int] = [0] * len(self.agent_wrappers)
        step: int = 0
        while True:
            step += 1
            all_is_terminated: bool = True
            for agent_warpper in self.agent_wrappers:
                agent: Agent = agent_warpper["agent"]
                agent_data: AgentData = agent_warpper["data"]

                if agent.is_terminated:
                    continue
                
                if "before_step" in self.callbacks: self.callbacks["before_step"](agent = agent, agent_data = agent_data, step = step)
                start_time = time.time()
                reward, action, action_index = self._take_one_step(agent, agent_data)
                duration = time.time() - start_time
                if "after_step" in self.callbacks: self.callbacks["after_step"](agent = agent, agent_data = agent_data, step = step, reward = reward, action = action, action_index = action_index, duration = duration)
                
                episode_rewards[agent.uuid] += reward

                if not agent.is_terminated:
                    all_is_terminated = False
            if all_is_terminated:
                return episode_rewards

    def _save_data(self, episode_rewards: List[int]) -> None:
        for index, agent_wrapper in enumerate(self.agent_wrappers):
            agent_data: AgentData = agent_wrapper["data"]
            reward: int = episode_rewards[index]
            if "reward" not in agent_data: agent_data["reward"] = []
            agent_data["reward"].append(reward)

            if "cumulative" not in agent_data: agent_data["cumulative"] = []
            previous_reward: int = agent_data["cumulative"][-1] if len(agent_data["cumulative"]) > 0 else 0
            cumulative_reward: int = reward + previous_reward
            agent_data["cumulative"].append(cumulative_reward)

    def _play(self, episodes: int) -> None:
        episode = 1
        while True:
            if "before_episode" in self.callbacks: self.callbacks["before_episode"](episode = episode)
            start_time = time.time()
            episode_rewards = self._continue_until_all_agents_is_terminated()
            duration = time.time() - start_time
            if "after_episode" in self.callbacks: self.callbacks["after_episode"](episode = episode, episode_rewards = episode_rewards, duration = duration)

            self._save_data(episode_rewards)
            agents = list(map(lambda item: item["agent"], self.agent_wrappers))
            self.api.reset(agents)
            episode += 1
            if episode > episodes:
                break

    def _performance(self, plot_performance: bool) -> None:
        for agent_index, agent_wrapper in enumerate(self.agent_wrappers):
            agent_data: AgentData = agent_wrapper["data"]
            if plot_performance:
                plot_agent_performance(agent_index, agent_data)

    def start(self, episodes: int, plot_performance: bool = False) -> None:
        self.api.print_environment_initial_data()
        self._play(episodes)
        self._performance(plot_performance)
