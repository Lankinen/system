from system.agent import Agent

from abc import ABC, abstractmethod
from typing import List, Tuple, Optional

class API(ABC):
    def __init__(self, number_of_agents: int):
        """Create own environment for each agent, or put all agents into the same environment.

        Args:
            number_of_agents (int): Number of agents
        """
        assert type(number_of_agents) is int

    @abstractmethod
    def get_state(self, agent: Agent) -> str:
        """Get state of the agent

        Args:
            agent (Agent): Agent

        Returns:
            str: current state
        """
        assert type(agent) is Agent or issubclass(type(agent), Agent)
        return "SV"

    @abstractmethod
    def get_action_options(self, agent: Agent) -> List[str]:
        """Get list of possible actions (might not be the same as all possible actions in the environment)

        Args:
            agent (Agent): Agent

        Returns:
            List[str]: list of possible actions
        """
        assert type(agent) is Agent or issubclass(type(agent), Agent)
        return ["left", "straight", "right"]

    @abstractmethod
    def do_action(self, agent: Agent, action_index: int) -> Tuple[int, Optional[str]]:
        """Do given action for the agent

        Args:
            agent (Agent): Agent
            action_index (int): index of the action (index is from action_options)

        Returns:
            Tuple[int, Optional[str]]: reward and next state (optional)
        """
        assert type(agent) is Agent or issubclass(type(agent), Agent)
        assert type(action_index) is int
        return 101, "mark"

    @abstractmethod
    def reset(self, agents: List[Agent]):
        """Reset agent terminate state to false and possibly do something with the environment.

        Args:
            agents (List[Agent]): List of agents to reset
        """
        for agent in agents:
            agent.is_terminated = False

    ### 👇 Not required to be implemented ###

    def print_environment_initial_data(self):
        """Print information about the environment (e.g. the optimal action)
        """
        print("no additional information about the environment")