import matplotlib.pyplot as plt

from typing import List, Union, Tuple, Dict

def print_progress(current: int, number_of_items: int, interval: int = 10) -> None:
    if current % interval == 0:
        print(str(current/number_of_items * 100) + "%")

def create_plot(x: List, y: List, x_label: str, y_label: str, title: str) -> None:
    plt.scatter(x, y, marker="+")
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.show()

def arg_max(items: List[int], value: bool = True) -> Union[Tuple[int, int], List[int]]:
    mx = max(enumerate(items), key=lambda x: x[1])
    if value:
        return mx
    return [i for i, e in enumerate(items) if e == mx[1]]