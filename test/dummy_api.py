from system.agent import Agent
from system.api import API

from typing import List, Tuple, Optional

class DummyAPI(API):
    def __init__(self, number_of_agents):
        super().__init__(number_of_agents)

    def get_state(self, agent: Agent) -> str:
        return super().get_state(agent)

    def get_action_options(self, agent: Agent) -> List[str]:
        return super().get_action_options(agent)

    def do_action(self, agent: Agent, action_index: int) -> Tuple[int, Optional[str]]:
        agent.is_terminated = True
        action_options = self.get_action_options(agent)
        if action_options[action_index] == "left":
            return 5, "samuli"
        if action_options[action_index] == "straight":
            return super().do_action(agent, action_index)
        return 10, "lankinen"

    def reset(self, agents: List[Agent]):
        super().reset(agents)