from test.dummy_agent import DummyAgent
from test.dummy_api import DummyAPI
from system.agent import Agent
from system.api import API
from system.rl import RL

from typing import List
import unittest

class TestRL(unittest.TestCase):
    agents: List[Agent]
    api: API
    rl: RL

    @classmethod
    def setUpClass(self) -> None:
        self.agents = [DummyAgent(), DummyAgent()]
        self.api = DummyAPI(len(self.agents))
        self.rl = RL(self.agents, self.api)

    def test_performance(self):
        self.rl._performance(False)

    def test_play(self):
        self.rl._play(2)

    def test_continue_until_all_agents_is_terminated(self):
        rewards = self.rl._continue_until_all_agents_is_terminated()
        self.assertEqual(rewards, [6, 0])

    def test_take_one_step(self):
        reward, action, action_index = self.rl._take_one_step(self.agents[0], {})
        self.assertEqual([reward, action, action_index], [3, "e", 13])