from system.agent import Agent
from test.dummy_agent import DummyAgent
from test.dummy_api import DummyAPI

from typing import List
import unittest

class TestAPI(unittest.TestCase):
    api: DummyAPI

    @classmethod
    def setUpClass(self) -> None:
        self.api = DummyAPI(2)

    def test_get_state(self) -> None:
        self.assertEqual(self.api.get_state(DummyAgent()), "SV")

    def test_get_action_options(self) -> None:
        ao: List[str] = self.api.get_action_options(DummyAgent())
        self.assertEqual(ao, ["left", "straight", "right"])

    def test_do_action(self) -> None:
        r1, s1 = self.api.do_action(DummyAgent(), 0)
        self.assertEqual(r1, 5)
        self.assertEqual(s1, "samuli")
        r2, s2 = self.api.do_action(DummyAgent(), 1)
        self.assertEqual(r2, 101)
        self.assertEqual(s2, "mark")
        r3, s3 = self.api.do_action(DummyAgent(), 2)
        self.assertEqual(r3, 10)
        self.assertEqual(s3, "lankinen")

    def test_reset(self) -> None:
        agents: List[Agent] = [DummyAgent(), DummyAgent()]
        self.assertFalse(agents[0].is_terminated)
        self.assertFalse(agents[1].is_terminated)
        self.api.reset(agents)
        self.assertFalse(agents[0].is_terminated)
        self.assertFalse(agents[1].is_terminated)

if __name__ == "__main__":
    unittest.main()