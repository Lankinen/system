from test.dummy_agent import DummyAgent

import unittest

class TestAgent(unittest.TestCase):
    agent: DummyAgent

    @classmethod
    def setUpClass(self) -> None:
        self.agent = DummyAgent()

    def test_init(self) -> None:
        self.agent = DummyAgent()
        self.assertFalse(self.agent.is_terminated)
    
    def test_do_step(self) -> None:
        reward, action, action_index = self.agent.do_step("state1", ["1","2","3"], lambda action: (1, None))
        self.assertEqual(reward, 3)
        self.assertEqual(action, "e")
        self.assertEqual(action_index, 13)

if __name__ == "__main__":
    unittest.main()