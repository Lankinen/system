from system.agent import Agent

from typing import Tuple, List, Callable, Optional

class DummyAgent(Agent):

    def __init__(self):
        super().__init__()

    def do_step(self, state: str, action_options: List[str], do_action: Callable[[int],Tuple[int, Optional[str]]]) -> Tuple[int, str, int]:
        reward, action, index_of_action = super().do_step(state, action_options, do_action)
        do_action(0)
        return reward, action, index_of_action