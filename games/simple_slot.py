from typing import List, Tuple
import random

class Casino():
    name: str
    levers: List[Tuple[int,int]]

    def __init__(self, name, number_of_levers = 3):
        self.name = name
        self.levers = []
        for _ in range(number_of_levers):
            range_min: int = random.randint(-20, 10)
            range_max: int = range_min + random.randint(1, 30)
            self.levers.append((range_min,range_max))

    def pull_lever(self, lever_index: int) -> int:
        lever = self.levers[lever_index]
        range_min = lever[0]
        range_max = lever[1]
        return random.randint(range_min, range_max)

class SimpleSlot():
    balance: int
    casinos: List[Casino]
    selected_casino: Casino

    def __init__(self, number_of_casinos=5):
        self.balance = 0
        self.casinos = []
        for index in range(number_of_casinos):
            casino = Casino(name = index, number_of_levers = random.randint(2,8))
            self.casinos.append(casino)
        self.selected_casino = self.casinos[0]
    
    def get_casino(self) -> Casino:
        return self.selected_casino
    
    def play_in_casino(self, lever_index: int) -> None:
        reward: int = self.selected_casino.pull_lever(lever_index)
        self.balance += reward
        self._change_to_random_casino()

    def _change_to_random_casino(self) -> None:
        random_index: int = random.randint(0,len(self.casinos)-1)
        self.selected_casino = self.casinos[random_index]