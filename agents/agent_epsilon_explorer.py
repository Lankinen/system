from system.agent import Agent
from system.utils import arg_max

import random
from typing import Callable, List, Tuple, Dict, Optional

class EpsilonExplorer(Agent):
    epsilon: float
    N: Dict
    Q: Dict

    def __init__(self, epsilon):
        super().__init__()
        self.epsilon = epsilon
        self.N = {}
        self.Q = {}

    def do_step(self, state: str, action_options: List[str], do_action: Callable[[int],Tuple[int, Optional[str]]]) -> Tuple[int, str, int]:
        super().do_step(state, action_options, do_action)
        for action in action_options:
            if state not in self.Q:
                self.Q[state] = {}
                self.N[state] = {}
                if action not in self.Q[state]:
                    self.Q[state][action] = 0
                    self.N[state][action] = 0
            else:
                if action not in self.Q[state]:
                    self.Q[state][action] = 0
                    self.N[state][action] = 0

        action_values = []
        for action in action_options:
            q = self.Q[state][action]
            n = self.N[state][action]
            if n == 0:
                action_values.append(0)
            else:
                action_values.append(q / n)

        _action_index = random.randint(0,len(action_options)-1)

        if random.uniform(0,1) > self.epsilon:
            _action_index = random.choice(arg_max(action_values, value=False))
        reward, _ = do_action(_action_index)
        selected_action = action_options[_action_index]
        self.Q[state][selected_action] += reward
        self.N[state][selected_action] += 1
        return reward, selected_action, _action_index